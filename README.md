## Build

```
$ bash run.sh
```

## Install

Copy Signal-Desktop somewhere in your ```$PATH```:

```
mv Signal-5.35.0.AppImage ~/.local/bin/Signal-Desktop.AppImage
```

(optional) You can also install the ```.desktop``` file

```
cp signal.desktop ~/.local/share/applications
```